// copyright anssi.grohn@karelia.fi 2019

using UnrealBuildTool;
using System.Collections.Generic;

public class EscapeInSpaceEditorTarget : TargetRules
{
	public EscapeInSpaceEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "EscapeInSpace" } );
	}
}
