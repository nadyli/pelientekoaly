#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FileReader.generated.h"

UCLASS()
class ESCAPEINSPACE_API UFileReader : public UBlueprintFunctionLibrary {
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Load file")
	static bool FileLoadString(FString FileNameA, FString& LoadedTextA);
};