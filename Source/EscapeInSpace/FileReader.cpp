#include "FileReader.h"
#include "EscapeInSpace.h"
#include "Misc/Paths.h"

bool UFileReader::FileLoadString(FString FileNameA, FString& LoadedTextA) {
	return FFileHelper::LoadFileToString(LoadedTextA, *(FPaths::ProjectDir() + FileNameA));
}