// copyright anssi.grohn@karelia.fi 2019

#include "EscapeInSpace.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, EscapeInSpace, "EscapeInSpace" );
