// copyright anssi.grohn@karelia.fi 2019

using UnrealBuildTool;
using System.Collections.Generic;

public class EscapeInSpaceTarget : TargetRules
{
	public EscapeInSpaceTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "EscapeInSpace" } );
	}
}
